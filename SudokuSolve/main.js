//function solve()
//{
	if (window.location.href.indexOf("sudoku.tagesspiegel.de") === -1)
	{
		window.location="https://sudoku.tagesspiegel.de";
		//alert("Navigating to site. Click again!");
	}
	
	else
	{
		//Click start button, if visible
		var b = document.querySelector("#overlay-wrapper .sudoku-start-btn");
		if (b != null)
			b.click();
		
		var e = document.querySelectorAll("#overlay-wrapper .sudoku .empty");
		
		var eLength = e.length;
		for (i = 0; i < eLength; i++)
		{
			e[i].innerHTML = e[i].getAttribute("data-solution");
		}
		
		//game does not recognize, that we filled every space
		//so we need to click the right solution manually for one space
		e[0].click();
		var keyboardNrs = document.querySelectorAll("#numberKeyboard .dbtn.number");
		var solutionNr = parseInt(e[0].getAttribute("data-solution"));
		
		var keyboardNrsLength = keyboardNrs.length;
		for (i = 0; i < keyboardNrsLength; i++)
		{
			if (keyboardNrs[i].innerHTML.includes(solutionNr))
			{
				keyboardNrs[i].click();
				break;
			}
		}
	}
//}
